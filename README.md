# LeoRV32 Verification

This repository contains the verification environment for the LeoRV32 RISC-V CPU.

The compliance tests are from the [riscv-arch-test](https://github.com/riscv-non-isa/riscv-arch-test) repository.

A report showing the successful completion of the test suite for `RV32I` can be found under `success/report.html`.

## Dependencies

First, make sure to pull the submodules:

	git submodule update --init

Next, install `riscof`, preferably via pip:

	pip3 install riscof

To clone the `riscv-arch-test` repo, run:

	riscof --verbose info arch-test --clone

The SAIL C-emulator is used as reference model. Next, clone the `riscof-plugin` repository that contains the `sail_cSim` plugin:

	git clone https://gitlab.com/incoresemi/riscof-plugins.git

Now sail can be installed either manually or via docker. The `config.ini` is set up to use the docker image. The container is automatically pulled on running the tests.

## Running the tests

In order to compile the firmware you need a RISC-V toolchain in your `PATH` variable. For these tests `riscv32-unknown-elf-` is used.

To run the tests, execute:

	riscof run --config ./config.ini --suite ./riscv-arch-test/riscv-test-suite/rv32i_m --env ./riscv-arch-test/riscv-test-suite/env

Afterwards `riscof_work/report.html` should contain the test results.