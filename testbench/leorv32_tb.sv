// SPDX-FileCopyrightText: © 2022 Leo Moser <https://codeberg.org/mole99>
// SPDX-License-Identifier: GPL-3.0-or-later

module leorv32_tb;

    logic clk;
    logic reset;

    // Instruction Port
    logic [31: 0] instr_addr;   // address
    logic [31: 0] instr_rdata;  // read data
    logic         instr_fetch;  // read
    logic         instr_done;   // done

    // Data Port
    logic [31: 0] data_addr;   // address
    logic [31: 0] data_wdata;  // write data
    logic [ 3: 0] data_wmask;  // write mask
    logic [31: 0] data_rdata;  // read data
    logic         data_rstrb;  // read strobe
    logic         data_wstrb;  // write strobe
    logic         data_done;   // read busy

    leorv32 #(
        .RESET_ADDR (32'h00000000),
        .ADDR_WIDTH (32),
        .MHARTID    (0)
    ) leorv32_2_inst (
        .clk,
        .reset,

        // Instruction Port
        .instr_addr,   // address
        .instr_rdata,  // read data
        .instr_fetch,  // read
        .instr_done,   // done

        // Data Port
        .data_addr,   // address
        .data_wdata,  // write data
        .data_wmask,  // write mask
        .data_rdata,  // read data
        .data_rstrb,  // read strobe
        .data_wstrb,  // write strobe
        .data_done,   // read busy

        .mhartid_0  ('0) // ored with the last bit of MAHRTID
    );

    logic [31: 0] mem_addr;
    logic [31: 0] mem_wdata;
    logic [ 3: 0] mem_wmask;
    logic         mem_wstrb;
    logic [31: 0] mem_rdata;
    logic         mem_rstrb;

    assign mem_addr = instr_fetch ? instr_addr : data_addr;
    assign mem_rstrb = instr_fetch || data_rstrb;
    
    // Fetch
    assign instr_rdata = mem_rdata;
    assign instr_done = 1'b1;
    
    // Load / Store
    assign mem_wdata = data_wdata;
    assign mem_wmask = data_wmask;
    assign mem_wstrb = data_wstrb;
    assign data_rdata = mem_rdata;
    assign data_done = 1'b1;

    // Memory

    localparam MEMORY_WIDTH = 21;

    logic [31:0] memory[0:(2**MEMORY_WIDTH)-1];
    wire [MEMORY_WIDTH-3:0] ram_word_address = mem_addr[MEMORY_WIDTH-1:2];

    always @(posedge clk) begin
    
        if (mem_wstrb) begin
            //$display("write @ %h/%h: %h wmask %d", mem_addr, ram_word_address, mem_wdata, mem_wmask);
            if (mem_wmask[0]) memory[ram_word_address][ 7:0 ] <= mem_wdata[ 7:0 ];
            if (mem_wmask[1]) memory[ram_word_address][15:8 ] <= mem_wdata[15:8 ];
            if (mem_wmask[2]) memory[ram_word_address][23:16] <= mem_wdata[23:16];
            if (mem_wmask[3]) memory[ram_word_address][31:24] <= mem_wdata[31:24];
        end
        
        if (mem_rstrb) begin
            mem_rdata <= memory[ram_word_address];
            //$display("read @ %h: %h", ram_word_address, memory[ram_word_address]);
        end
    end
    
    // Testbench data
    string isa;
    string signature;
    string hex_file;
    logic [31:0] begin_signature;
    logic [31:0] end_signature;
    logic [31:0] tohost;
    int signature_granularity;
    integer file;

    // Read plusargs
    initial begin
        $display("Starting simulation!");
        
        if (! $value$plusargs("isa=%s", isa)) begin
            $display("ERROR: please specify +isa=<value>");
            $finish;
        end
        
        if (! $value$plusargs("signature=%s", signature)) begin
            $display("ERROR: please specify +signature=<value>");
            $finish;
        end
        
        if (! $value$plusargs("hex_file=%s", hex_file)) begin
            $display("ERROR: please specify +hex_file=<value>");
            $finish;
        end
        
        if (! $value$plusargs("signature-granularity=%d", signature_granularity)) begin
            $display("ERROR: please specify +signature-granularity=<value>");
            $finish;
        end
        
        if (! $value$plusargs("begin_signature=%h", begin_signature)) begin
            $display("ERROR: please specify +begin_signature=<value>");
            $finish;
        end
        
        if (! $value$plusargs("end_signature=%h", end_signature)) begin
            $display("ERROR: please specify +end_signature=<value>");
            $finish;
        end
        
        if (! $value$plusargs("tohost=%h", tohost)) begin
            $display("ERROR: please specify +tohost=<value>");
            $finish;
        end
        
        $display("isa=%s signature=%s hex_file=%s signature-granularity=%d begin_signature=%h end_signature=%h tohost=%h", isa, signature, hex_file, signature_granularity, begin_signature, end_signature, tohost);
        
        // Dump waveforms
        $dumpfile("dump.fst");
        $dumpvars(0, leorv32_tb);
        for (int i=0; i<32; i++) $dumpvars(0, leorv32_2_inst.leorv32_regs_inst.regs[i]);
        
        // Initialize memory
        $readmemh(hex_file, memory);
    end

    // Reset
    initial begin
        clk = 1'b0;
        reset = 1'b1;
        #10;
        reset = 1'b0;    
    end
    
    // Toggle clock
    always begin
        #1 clk = !clk;
    end
    
    localparam TIMEOUT = 100000*2;
    
    // Timeout
    always begin
        #TIMEOUT;
        $display("Simulation timeout!");
        file = $fopen(signature,"w");

        for (int i=begin_signature; i<end_signature; i=i+4) begin
            $display("byte_addr=%h data=%h", i, memory[i>>2]);
            $fwrite(file,"%h\n", memory[i>>2]);
        end

        $fclose(file);
        $finish;
    end
    
    // Succesfull simulation
    always begin
        @(posedge clk);
        if (mem_wmask == 4'hF && mem_wdata == 32'h1 && mem_addr == tohost) begin
            $display("Simulation completed!");
            file = $fopen(signature,"w");

            for (int i=begin_signature; i<end_signature; i=i+4) begin
                $display("byte_addr=%h data=%h", i, memory[i>>2]);
                $fwrite(file,"%h\n", memory[i>>2]);
            end

            $fclose(file);
            $finish;
        end
    end

endmodule
