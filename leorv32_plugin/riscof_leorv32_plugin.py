import os
import re
import shutil
import subprocess
import shlex
import logging
import random
import string
from string import Template
import sys

import riscof.utils as utils
import riscof.constants as constants
from riscof.pluginTemplate import pluginTemplate

logger = logging.getLogger()

class leorv32_plugin(pluginTemplate):
    __model__ = "leorv32"
    __version__ = "0.0.1"

    def __init__(self, *args, **kwargs):
        sclass = super().__init__(*args, **kwargs)

        config = kwargs.get('config')

        # If the config node for this DUT is missing or empty. Raise an error. At minimum we need
        # the paths to the ispec and pspec files
        if config is None:
            print("Please enter input file paths in configuration.")
            raise SystemExit(1)

        # In case of an RTL based DUT, this would be point to the final binary executable of your
        # test-bench produced by a simulator (like verilator, vcs, incisive, etc). In case of an iss or
        # emulator, this variable could point to where the iss binary is located. If 'PATH variable
        # is missing in the config.ini we can hardcode the alternate here.
        self.dut_exe = 'iverilog -o leorv32.vvp -g2012 {0}/leorv32_pkg.sv {0}/leorv32.sv {1}/leorv32_tb.sv'.format(os.path.abspath(config['rtlPath']), os.path.abspath(config['testbenchPath']))
        self.sim_exe = 'vvp {0}/leorv32.vvp -fst'

        self.tools_path = os.path.abspath(config['toolsPath'])

        # Number of parallel jobs that can be spawned off by RISCOF
        # for various actions performed in later functions, specifically to run the tests in
        # parallel on the DUT executable. Can also be used in the build function if required.
        self.num_jobs = str(config['jobs'] if 'jobs' in config else 1)

        # Path to the directory where this python file is located. Collect it from the config.ini
        self.pluginpath=os.path.abspath(config['pluginpath'])

        # Collect the paths to the  riscv-config absed ISA and platform yaml files. One can choose
        # to hardcode these here itself instead of picking it from the config.ini file.
        self.isa_spec = os.path.abspath(config['ispec'])
        self.platform_spec = os.path.abspath(config['pspec'])

        #We capture if the user would like the run the tests on the target or
        #not. If you are interested in just compiling the tests and not running
        #them on the target, then following variable should be set to False
        if 'target_run' in config and config['target_run']=='0':
            self.target_run = False
        else:
            self.target_run = True

        # Return the parameters set above back to RISCOF for further processing.
        return sclass

    def initialise(self, suite, work_dir, archtest_env):

       # capture the working directory. Any artifacts that the DUT creates should be placed in this
       # directory. Other artifacts from the framework and the Reference plugin will also be placed
       # here itself.
       self.work_dir = work_dir

       # capture the architectural test-suite directory.
       self.suite_dir = suite
       
       # capture the archtest directory.
       self.archtest_env = archtest_env

    def build(self, isa_yaml, platform_yaml):

      # load the isa yaml as a dictionary in python.
      self.ispec = utils.load_yaml(isa_yaml)['hart0']

      # capture the XLEN value by picking the max value in 'supported_xlen' field of isa yaml. This
      # will be useful in setting integer value in the compiler string (if not already hardcoded);
      self.xlen = ('64' if 64 in self.ispec['supported_xlen'] else '32')

      # for leorv32 start building the '--isa' argument. the self.isa is dutnmae specific and may not be
      # useful for all DUTs
      self.isa = 'rv' + self.xlen
      if "I" in self.ispec["ISA"]:
          self.isa += 'i'
      if "M" in self.ispec["ISA"]:
          self.isa += 'm'
      if "F" in self.ispec["ISA"]:
          self.isa += 'f'
      if "D" in self.ispec["ISA"]:
          self.isa += 'd'
      if "C" in self.ispec["ISA"]:
          self.isa += 'c'
      
      # compile the rtl files
      logger.info('Compiling leorv32: ' + self.dut_exe)
      utils.shellCommand(self.dut_exe).run(cwd=self.work_dir)

    def runTests(self, testList):

      # we will iterate over each entry in the testList. Each entry node will be refered to by the
      # variable testname.
      for testname in testList:

          # for each testname we get all its fields (as described by the testList format)
          testentry = testList[testname]

          # we capture the path to the assembly file of this test
          test = testentry['test_path']

          # capture the directory where the artifacts of this test will be dumped/created. RISCOF is
          # going to look into this directory for the signature files
          test_dir = testentry['work_dir']

          # name of the elf file after compilation of the test
          elf_file = 'dut.elf'
          bin_file = 'dut.bin'
          hex_file = 'dut.hex'

          # name of the signature file as per requirement of RISCOF. RISCOF expects the signature to
          # be named as DUT-<dut-name>.signature. The below variable creates an absolute path of
          # signature file.
          sig_file = os.path.join(test_dir, self.name[:-1] + ".signature")

          # for each test there are specific compile macros that need to be enabled. The macros in
          # the testList node only contain the macros/values. For the gcc toolchain we need to
          # prefix with "-D". The following does precisely that.
          compile_macros= ' -D' + " -D".join(testentry['macros'])
          
          # Note the march is not hardwired here, because it will change for each
          # test. Similarly the output elf name and compile macros will be assigned later in the
          # runTests function
          self.compile_cmd = ('riscv{1}-unknown-elf-gcc -march={0} \
            -static -mcmodel=medany -fvisibility=hidden -nostdlib -nostartfiles -g\
            -T '+self.pluginpath+'/env/link.ld\
            -I '+self.pluginpath+'/env/\
            -I ' + self.archtest_env + ' {2} -o {3} {4}').format(testentry['isa'].lower(), self.xlen, test, elf_file, compile_macros)
            
          self.compile_cmd += ' -mabi='+('lp64 ' if 64 in self.ispec['supported_xlen'] else 'ilp32 ')
          self.compile_cmd += ' && riscv{0}-unknown-elf-objcopy -O binary {1} {2}'.format(self.xlen, elf_file, bin_file)
          self.compile_cmd += ' && python3 ' + self.tools_path +'/makehex.py {0} {1} > {2}'.format(bin_file, 2**22,hex_file)

          cmd = self.compile_cmd
          
          # just a simple logger statement that shows up on the terminal
          logger.info('Compiling test: ' + test + " with " + cmd)
          
          # the following command spawns a process to run the compile command. Note here, we are
          # changing the directory for this command to that pointed by test_dir. If you would like
          # the artifacts to be dumped else where change the test_dir variable to the path of your
          # choice.
          utils.shellCommand(cmd).run(cwd=test_dir)

          # if the user wants to disable running the tests and only compile the tests, then
          # the if condition is skipped
          if self.target_run:
            
            get_begin_signature = "$(readelf "+testentry['work_dir']+"/dut.elf --symbols | grep rvtest_sig_begin | awk '{print $2}')"
            get_end_signature   = "$(readelf "+testentry['work_dir']+"/dut.elf --symbols | grep rvtest_sig_end | awk '{print $2}')"
            get_tohost          = "$(readelf "+testentry['work_dir']+"/dut.elf --symbols | grep tohost | awk '{print $2}')"
            
            simcmd = self.sim_exe.format(self.work_dir) + ' +isa={0} +signature={1} +signature-granularity=4 +hex_file={2} +begin_signature={3} +end_signature={4} +tohost={5}'.format(self.isa, sig_file, hex_file, get_begin_signature, get_end_signature, get_tohost)
            logger.info('Running:' + simcmd)

            # launch the execute command. Change the test_dir if required.
            utils.shellCommand(simcmd).run(cwd=test_dir)


      # if target runs are not required then we simply exit as this point after running all
      # the makefile targets.
      if not self.target_run:
        raise SystemExit

